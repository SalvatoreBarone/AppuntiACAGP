/**
 * @file prod_mv.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of "A Project"
 *
 * "A Project" is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * "A Project" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */


#include <iostream>
#include <exception>
#include <limits>
#include <iomanip>

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <cstring>

#include <unistd.h>

#include "omp.h"
#include "mv.hpp"

using namespace std;

#define DEFAULT "\e[0m"
#define RED 	"\e[1;31m"
#define GREEN 	"\e[1;32m"
#define YELLOW 	"\e[1;33m"
#define BLUE 	"\e[1;34m"
#define MAGENTA "\e[1;35m"
#define CIAN 	"\e[1;36m"


/**
 * Controllo correttezza dell'input dato al programma
 *
 * @param argc numero di parametri dati al programma
 * @param argv array di puntatori ai parametri dati al programma
 * @param nthread numero di thread coinvolti nel calcolo
 * @param func puntatore alla funzione da integrare
 * @param a estremo inferiore dell'intervallo di integrazione
 * @param b estremo superiore dell'intervallo di integrazione
 * @param n_int numero di intervalli
 * @return
 */
bool input_check(	int argc,
					char** argv,
					int& nthread,
					unsigned& rows,
					unsigned& cols,
					bool& verbose);

void print_usage(char* argv0);


int main (int argc, char** argv)
{
	int num_thread = 0;
	unsigned rows = 0, cols = 0;
	bool verbose = false;

	if (!input_check(argc, argv, num_thread, rows, cols, verbose))
		return 0;

	srand(time(NULL));
	
	// allocazione di matrice e vettore
	float** mtx = new float* [rows];
	for (unsigned i = 0; i < rows; i++)
		mtx[i] = new float[cols];
	float* vett = new float[cols];
	float* prod = new float[rows];
	mtx_init(mtx, rows, cols);
	vett_init(vett, cols);

	
	unsigned i, j;
	omp_set_num_threads(num_thread);
	
	double enlapsed_time = omp_get_wtime();
#ifdef __V1__	
	#pragma omp parallel \
		default(none) \
		private(i, j) \
		shared(rows, cols, prod, mtx, vett)
	{
		#pragma omp for private (i, j)
		for (i = 0; i < rows; i++)
		{
			prod[i] = 0;
			for (j = 0; j < cols; j++)
				prod[i] += mtx[i][j] * vett[j];
		}
	}
#endif

#ifdef __V2__
	float tmp = 0;
	#pragma omp parallel \
		default(none) \
		private(i, j, tmp) \
		shared(rows, cols, prod, mtx, vett)
	{
		#pragma omp for private (i, j, tmp)
		for (i = 0; i < rows; i++) 
		{
			tmp = 0;
			for (j = 0; j < cols; j++)
				tmp += mtx[i][j] * vett[j];
			prod[i] = tmp;
		}
	}
#endif

	enlapsed_time = (omp_get_wtime() - enlapsed_time) * 1000000000;

	if (verbose) {
		cout <<"righe=" <<rows << " colonne=" << cols << " ncpu" <<num_thread << endl;
		cout << "matrice:" << endl;
		mtx_print(mtx, rows, cols);
		cout << "vettore:" << endl;
		vett_print(vett, cols);
		cout << "prodotto:" << endl;
		vett_print(prod, rows);
		cout << endl << endl << " tempo: ";
	}

	cout << fixed << enlapsed_time << " ns" << endl;

	for (unsigned i = 0; i < rows; i++)
		delete [] mtx[i];
	delete [] mtx;
	delete [] vett;
	delete [] prod;
}


bool input_check(	int argc,
					char** argv,
					int& nthread,
					unsigned& rows,
					unsigned& cols,
					bool& verbose)
{
	
	int par;
	while((par = getopt(argc, argv, ":t:r:c:v")) != -1) {
		switch (par) {
			case 't' : 
				nthread = strtoul(optarg, NULL, 0);
				break;
			
			case 'r' :
				rows = strtoul(optarg, NULL, 0);
				break;
			
			case 'c' :
				cols = strtoul(optarg, NULL, 0);
				break;
			
			case 'v' : 
				verbose = true;
				break;
			
			default:
				cout << par <<": parametro non riconosciuto." << endl;
				print_usage(argv[0]);
			break;
		}
	}
	
	
	if (nthread == 0) {
		cout << RED << "Specificare il numero di threads!" << DEFAULT << endl << endl;
		print_usage(argv[0]);
		return false;
	}
	
	if (rows == 0) {
		cout << RED << "Specificare il numero di righe!" << DEFAULT << endl << endl;
		print_usage(argv[0]);
		return false;
	}
	
	if (cols == 0) {
		cout << RED << "Specificare il numero di colonne!"  << DEFAULT << endl << endl;
		print_usage(argv[0]);
	}
	
	return true;
}

void print_usage(char* argv0) {
	cout	<< "Uso: " <<argv0 <<" -t nthreads -t nthreads -r righe -c colonne [-v]" << endl
			<< "Esempio: " <<argv0 <<" -t 4 -r 1000 -c 1000" << endl
			<< "L'opzione -v abilita la stampa messaggi di debug." << endl;
}

