/**
 * @file integrale.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of "A Project"
 *
 * "A Project" is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * "A Project" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */


#include <iostream>
#include <exception>
#include <limits>

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <cstring>

#include <assert.h>
#include <fcntl.h>
#include <unistd.h>

#include "omp.h"

#define DEFAULT "\e[0m"
#define RED 	"\e[1;31m"
#define GREEN 	"\e[1;32m"
#define YELLOW 	"\e[1;33m"
#define BLUE 	"\e[1;34m"
#define MAGENTA "\e[1;35m"
#define CIAN 	"\e[1;36m"



using namespace std;

typedef double(*func_ptr)(double);	// per comodita' definiamo un puntatore a funzione reale
									// di variabile reale
									
void print_usage(char* argv0);

/**
 * Controllo correttezza dell'input dato al programma
 *
 * @param argc numero di parametri dati al programma
 * @param argv array di puntatori ai parametri dati al programma
 * @param nthread numero di thread coinvolti nel calcolo
 * @param func puntatore alla funzione da integrare
 * @param a estremo inferiore dell'intervallo di integrazione
 * @param b estremo superiore dell'intervallo di integrazione
 * @param n_int numero di intervalli
 * @return
 */
void input_check(	int argc,
					char** argv,
					int& nthreads,
					func_ptr& func,
					double &a,
					double &b,
					int &n_int,
					bool &verbose);

/**
 * @brief
 * 
 * @param[in] par
 * 
 * @return
 */
func_ptr get_function(char* par);

int main (int argc, char** argv)
{
	double a = 0, b = 0;
	int n_int = 0;
	int nthreads = 0;
	func_ptr func = NULL;
	bool verbose = false;
	
	input_check(argc, argv, nthreads, func, a, b, n_int, verbose);

	// inizio della regione parallela
	double h = (b - a) / n_int;
	int i;
	double partial = 0;
	double enlapsed_time = omp_get_wtime();
	
	assert(func != NULL);
	assert(nthreads);
	assert(n_int);

	omp_set_num_threads(nthreads);

#ifdef __V1__	
	#pragma omp parallel reduction(+:partial) 
	{
		#pragma omp for private(i)
			for (i = 0; i < n_int; i++)
				partial += func(a + float(i * h));
		partial *= h;
	}
#endif

#ifdef __V2__
	double tmp = 0;
	#pragma omp parallel \
		default(none) \
		shared(a, b, h, n_int, func) \
		private (i) \
		firstprivate(tmp) \
		reduction(+:partial)
	{
		#pragma omp for private(i)
			for (i = 0; i < (int)n_int; i++)
				tmp += func(a + float(i * h));
			partial = tmp * h;
	}
#endif
	
	partial  += (func(a) + func(b)) * (b - a) / 2 / n_int;
	enlapsed_time = (omp_get_wtime() - enlapsed_time) * 1000000000;

	// la stampa dei tempi verra' eseguita usando tutta la precisione possibile
	typedef std::numeric_limits<double> dbl;
	cout.precision(dbl::digits10);
	if (verbose) {
		cout << "numero thread: " << nthreads << endl;
		cout << "numero intervalli: " << n_int << endl;
		cout << "a: " << a << endl;
		cout << "b: " << b << endl;
		cout << "totale: " << partial << endl;
		cout << "tempo: " << fixed << enlapsed_time << " ns" << endl;
	}
	else
		cout << fixed << enlapsed_time << endl;
}

void print_usage(char* argv) {
	cout	<< "Uso: " <<argv <<" -t nthreads -f function -n intervals -a a -b b" << endl
			<< "Esempio: " <<argv <<" -t 4 -f sin -n 10000 -a 12.6 -b 33.45" << endl
			<< endl
			<< "Funzioni supportate: " << endl
			<< "\tsin: seno" << endl
			<< "\tcos: coseno" << endl
			<< "\ttan: tangente" << endl
			<< "\tlog: logaritmo in base 10" << endl
			<< "\tln: logaritmo in base e" << endl
			<< "\texp: esponenziale" << endl;
}

void input_check(	int argc,
					char** argv,
					int& nthreads,
					func_ptr& func,
					double &a,
					double &b,
					int &n_int,
					bool &verbose)
{
	int par;
	while((par = getopt(argc, argv, "f:t:a:b:n:v")) != -1) {
		switch (par) {
			case 'f' : 
				func = get_function(optarg);
				break;
			
			case 't' : 
				nthreads = strtoul(optarg, NULL, 0);
				break;
			
			case 'a' :
				a = strtod(optarg, NULL);
				break;
			
			case 'b' :
				b = strtod(optarg, NULL);
				break;
			
			case 'n' : 
				n_int = strtoul(optarg, NULL, 0);
				break;
			
			case 'v' : 
				verbose = true;
				break;
			
			default:
				cout << par <<": parametro non riconosciuto." << endl;
				print_usage(argv[0]);
			break;
		}
	}
	
	if (func == NULL) {
		cout << RED << "Specificare una funzione!" << DEFAULT << endl << endl;
		print_usage(argv[0]);
		return;
	}
	
	if (nthreads == 0) {
		cout << RED << "Specificare il numero di threads!" << DEFAULT << endl << endl;
		print_usage(argv[0]);
		return;
	}
	
	if (n_int == 0) {
		cout << RED << "Specificare il numero di intervalli!" << DEFAULT << endl << endl;
		print_usage(argv[0]);
		return;
	}
	
}

func_ptr get_function(char* par) {
	if (strcmp(par, "sin") == 0)
		return sin;
	
	if (strcmp(par, "cos") == 0)
		return cos;
		
	if (strcmp(par, "tan") == 0)
		return tan;
		
	if (strcmp(par, "log") == 0)
		return log10;
		
	if (strcmp(par, "ln") == 0)
		return log;
		
	if (strcmp(par, "exp") == 0)
		return exp;

	return NULL;
}
