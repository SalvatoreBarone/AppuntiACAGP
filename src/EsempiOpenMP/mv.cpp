/**
 * @file mv.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of "A Project"
 *
 * "A Project" is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * "A Project" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "mv.hpp"
#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

void mtx_init(float** mtx, unsigned rows, unsigned cols) {
	for (unsigned int i = 0; i < rows; i++)
		for (unsigned int j = 0; j < cols; j++)
		{
			float num = rand(), den = rand();
			mtx[i][j] = num / den;
		}
}

void mtx_print(float** mtx, unsigned rows, unsigned cols) {
	for (unsigned int i = 0; i < rows; i++)
	{
		for (unsigned int j = 0; j < cols; j++)
			cout << setw(15) << mtx[i][j] << "\t";
		cout << endl;
	}
	cout << endl;
}

void vett_init(float* vett, unsigned size) {
	for (unsigned int i = 0; i < size; i++)
	{
		float num = rand(), den = rand();
		vett[i] = num / den;
	}
}

void vett_print(float* vett, unsigned size) {
	for (unsigned int i = 0; i < size; i++)
		cout << setw(15) << vett[i] << "\t";
	cout << endl << endl;
}

