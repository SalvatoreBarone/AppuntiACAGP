/**
 * @file mv.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of "A Project"
 *
 * "A Project" is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * "A Project" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
 
#ifndef __MV__HEADER__
#define __MV__HEADER__

void mtx_init(float** mtx, unsigned rows, unsigned cols);
void mtx_print(float** mtx, unsigned rows, unsigned cols);
void vett_init(float* vett, unsigned size);
void vett_print(float* vett, unsigned size);

#endif
