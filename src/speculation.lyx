#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass book
\begin_preamble
\frenchspacing
\usepackage{hyperref}
\usepackage{amsmath}
\newcommand{\ø}{\raisebox{.3ex}{\tiny$\; \bullet \;$}}
\renewcommand{\arraystretch}{1.25}
\usepackage{mathpazo}
\usepackage{color}
\usepackage{graphicx}
\usepackage{multicol}

\RequirePackage[dvipsnames]{xcolor} % [dvipsnames]
\definecolor{halfgray}{gray}{0.55} % chapter numbers will be semi transparent .5 .55 .6 .0
\definecolor{webgreen}{rgb}{0,.5,0}
\definecolor{webbrown}{rgb}{.6,0,0}
\definecolor{Maroon}{cmyk}{0, 0.87, 0.68, 0.32}
\definecolor{RoyalBlue}{cmyk}{1, 0.50, 0, 0}
\definecolor{Black}{cmyk}{0, 0, 0, 0}
\newcommand{\stitle}[1]{{\textbf{\color{RoyalBlue} #1}}}
\newcommand{\mail} [1]{{\href{mailto:#1}{#1}}}

\hypersetup{
    colorlinks=true, linktocpage=true, pdfstartpage=3, pdfstartview=FitV,%
    breaklinks=true, pdfpagemode=UseNone, pageanchor=true, pdfpagemode=UseOutlines,
    plainpages=false, bookmarksnumbered, bookmarksopen=true, bookmarksopenlevel=1,
    hypertexnames=true, pdfhighlight=/O,
    urlcolor=RoyalBlue, linkcolor=RoyalBlue, 
    citecolor=webgreen, 
    pdfauthor = {NOME COGNOME},
    pdfcreator  = {LaTeX with hyperref package},
    pdfproducer = {pdfeTeX},
    plainpages=false,
    pdfpagelabels
}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language italian
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 1
\bibtex_command default
\index_command default
\paperfontsize 10
\spacing single
\use_hyperref false
\papersize a4paper
\use_geometry false
\use_package amsmath 1
\use_package amssymb 2
\use_package cancel 1
\use_package esint 0
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 0
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Indice
\shortcut idx
\color #008000
\end_index
\leftmargin 3cm
\topmargin 3cm
\rightmargin 3cm
\bottommargin 3cm
\headsep 1cm
\secnumdepth 3
\tocdepth 4
\paragraph_separation skip
\defskip medskip
\quotes_language swedish
\papercolumns 1
\papersides 2
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Section
Introduzione
\end_layout

\begin_layout Standard
I concetti alla base della hardware-speculation sono tre:
\end_layout

\begin_layout Enumerate
branch-prediction, per predeterminare quale ramo di un branch seguire;
\end_layout

\begin_layout Enumerate
speculazioni sulle istruzioni, per permettere l'esecuzione delle istruzioni
 prima che le dipendenze di controllo siano risolte, facendo affidamento
 sul fatto che gli effetti dell'esecuzione di sequenze speculative non corrette
 possano essere annullati;
\end_layout

\begin_layout Enumerate
dynamic-scheduling delle istruzioni, anche per quelle si trovino dopo un'istruzi
one di branch.
\end_layout

\begin_layout Standard
Con l'hardware-speculation si esegue il flusso di istruzioni predetto basandosi
 sui dati disponibili: le operazioni vengono eseguite appena gli operandi
 sono disponibili con un modello di esecuzione molto simile a quello 
\emph on
data-flow
\emph default
.
\end_layout

\begin_layout Section
Branch prediction
\end_layout

\begin_layout Standard
In un sistema pipelined è molto importante riconoscere le istruzioni di
 salto il prima possibile, in modo da poter tentare di indovinare quale
 sia l'indirizzo della prossima istruzione da eseguire.
 Questa predizione può essere effettuata basandosi su considerazioni statistiche
 effettuate da un 
\series bold
\emph on
branch-predictor
\series default
\emph default
.
 È molto importante avere un branch-predictor accurato in quanto previsioni
 errate incidono molto sulle performance del sistema.
\end_layout

\begin_layout Subsection
Predittore a due bit
\end_layout

\begin_layout Standard
Il predittore classico utilizza quattro stati e prende il nome di predittore
 a 2-bit, o 
\emph on
saturating-counter-predictor
\emph default
.
 Il predittore utilizza una 
\emph on
branch-prediction table
\emph default
 in cui sono memorizzati, per ogni istruzione di salto, lo stato del predittore
 e l'indirizzo a cui saltare.
 Il suo funzionamento è molto semplice ed è basato sull'automa mostrato
 in figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:2-bit-branch-predictor"

\end_inset

.
\end_layout

\begin_layout Standard
Inizialmente il predittore si trova nello stato "
\series bold
\emph on
strongly-not-taken
\series default
\emph default
" (
\emph on
SNT
\emph default
, o non-saltare-forte).
 In questo stato, il predittore, fa in modo che il salto non venga seguito.
 Se la previsione si rivela corretta, ossia il salto non doveva essere eseguito,
 il predittore resta nello stato SNT, altrimenti si porta nello stato "
\series bold
\emph on
weakly-not-taken
\series default
\emph default
" (
\emph on
WNT
\emph default
, o non-saltare-debole).
\end_layout

\begin_layout Standard
Lo stato WNT serve ad aggiungere 
\emph on
inerzia
\emph default
 al predittore, facendo in modo che il predittore continui a fare in modo
 che il salto non venga eseguito.
 Se la decisione si rivela corretta, allora il predittore si riporta nello
 stato SNT, se, invece la previsione si rivela sbagliata, cioè era necessario
 seguire il salto, il predittore si porta nello stato "
\series bold
\emph on
weakly-taken
\series default
\emph default
" (
\emph on
WT
\emph default
, o salta-debole) ed inserisce l'indirizzo di destinazione del salto all'interno
 della branch-prediction table.
\end_layout

\begin_layout Standard
Quando si trova nello stato WT, il predittore, incontrando la stessa istruzione
 di salto, fa in modo che il salto venga seguito, usando l'indirizzo di
 destinazione memorizzato nella branch-prediction table.
 Se la decisione si rivela errata, il predittore si riporta nello stato
 WNT, mentre, se la decisione si rivela corretta, allora il predittore si
 porta nello stato 
\begin_inset Quotes sld
\end_inset


\series bold
\emph on
strongly-taken
\series default
\emph default

\begin_inset Quotes sld
\end_inset

 (
\emph on
ST
\emph default
, o salta-forte).
 Nello stato strongly-taken il predittore segue sempre i salti, ma se la
 decisione dovesse rivelarsi sbagliata si riporta nello stato WT.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\noindent
\align center
\begin_inset Graphics
	filename img/3_HardwareSpeculation/Branch_prediction_2bit.png
	scale 50

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:2-bit-branch-predictor"

\end_inset

2-bit branch predictor
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Correlating-branch-predictors
\end_layout

\begin_layout Standard
Il predittore a 2-bit consente di ridurre di molto la 
\emph on
branch-penality
\emph default
 attraverso la predizione dei salti, ma esistono alcuni casi, come i loop
 annidati, in cui è molto probabile che quel tipo di predittore sbagli molto
 frequentemente.
 Il caso dei loop annidati non è l'unico caso in cui il predittore sbaglia
 molto frequentemente.
 Si consideri il blocco di codice riportato in figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:branch-correlati"

\end_inset

.
 Si può osservare che il terzo branch non viene mai preso se i primi due,
 invece, vengono presi.
 Con un predittore a 2-bit - detto, per questo motivo, predittore locale
 - è impossibile tenere traccia di questa correlazione.
 I branch-predictor che utilizzano la storia globale per stabilire quale
 ramo del branch seguire vengono detti 
\emph on
correlating-branch-predictor,
\emph default
 o predittori a due livelli.
 Questo tipo di predittori utilizza uno shift-register di m-bit per tenere
 traccia nell'esito delle ultime 
\emph on
m
\emph default
 predizioni, ed utilizza il contenuto dello shift-register per indirizzare
 uno tra 
\begin_inset Formula $2^{m}$
\end_inset

 predittori locali, usati per stabilire se un branch debba essere seguito
 o meno.
 Si può dimostrare che, a regime, in assenza di correlazione tra i salti
 i 
\begin_inset Formula $2^{m}$
\end_inset

 predittori locali tenderanno ad assumere lo stesso stato, mentre, in presenza
 di correlazioni tra salti, tali predittori tenderanno ad assumere una configura
zione specifica, in maniera dipendente dalla correlazione esistente con
 i branch precedenti.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\noindent
\align center
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\noindent
\align center
\begin_inset Graphics
	filename img/3_HardwareSpeculation/code_example.png
	scale 50

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
linguaggio C
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset space \hfill{}
\end_inset


\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\noindent
\align center
\begin_inset Graphics
	filename img/3_HardwareSpeculation/code_example1.png
	scale 60

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
MIPS assembly
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:branch-correlati"

\end_inset

branch correlati
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\noindent
\align center
\begin_inset Graphics
	filename img/3_HardwareSpeculation/correlating_branch_predictors.png
	scale 80

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
correlating branch predictor
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Tournament-predictors
\end_layout

\begin_layout Standard
I tournament-predictor portano il concetto di correlare storia locale e
 globale al massimo livello, usando un predittore locale (come un saturating-cou
nter predictor) ed uno globale (come un correlating-predictor) simultaneamente.
 Quando bisogna prevedere l'esito di un branch, un choiche-predictor sceglie
 quale predittore, tra locale e globale, usare, scegliendo di volta in volta
 quello che stia 
\begin_inset Quotes sld
\end_inset

funzionando meglio
\begin_inset Quotes srd
\end_inset

, nel senso che ha un hit-rate più elevato.
\end_layout

\begin_layout Standard
Il branch predictor del processore ALpha-21264 (fig.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:tournament-predictor-alpha21264"

\end_inset

) usava
\end_layout

\begin_layout Itemize
un predittore locale, che teneva traccia della storia dei branch singolarmente;
 era organizzato su due livelli:
\end_layout

\begin_deeper
\begin_layout Itemize
il primo livello era costituito da 1024 entry da 10 bit, indirizzate a partire
 dal PC, rappresentanti la storia di un particolare branch;
\end_layout

\begin_layout Itemize
il secondo livello era costituito da 1024 saturating-counter predictor a
 tre bit, indirizzati usando un'entry del primo livello.
\end_layout

\begin_layout Standard
Il valore assunto dal particolare contatore selezionato costituiva il risultato
 della predizione.
\end_layout

\end_deeper
\begin_layout Itemize
un predittore globale, che, in sostanza, era un correlating-predictor costituito
 da 4096 entry da 2 bit, indirizzate a partire dalla storia globale dei
 branch, espressa su 12 bit.
 Ciascuna delle entry era un saturating-counter predictor il cui valore
 rappresentava l'esito della predizione;
\end_layout

\begin_layout Itemize
un choice-predictor, composto da 4096 entry da due bit, ciascuna delle quali
 era un saturating-counter predictor usato per scegliere quale dei due predittor
i usare; le entry venivano indirizzate a partire dalla storia globale dei
 branch, espressa su 12 bit.
\end_layout

\begin_layout Standard
Il valore del contatore contenuto in una delle entry del choice-predictor
 corrispondeva al predittore da usare.
 Il valore di conteggio veniva aggiornato in base agli esiti delle predizioni
 effettuate dai singoli predittori:
\end_layout

\begin_layout Itemize
se il predittore locale effettuava una previsione corretta, mentre quello
 globale aveva sbagliato, il valore del contatore veniva decrementato;
\end_layout

\begin_layout Itemize
se il predittore globale effettuava una previsione corretta, mentre quello
 locale aveva sbagliato, il valore del contatore veniva incrementato;
\end_layout

\begin_layout Itemize
se nessuno dei due predittori effettuava una previsione corretta, o la effettuav
ano entrambi, il valore del contatore veniva lasciato inalterato;
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\noindent
\align center
\begin_inset Graphics
	filename img/3_HardwareSpeculation/hybrid_branch_predictors.png
	scale 30

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:tournament-predictor-alpha21264"

\end_inset

tournament predictor del processore Alpha-21264
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Il reorder-buffer
\end_layout

\begin_layout Standard
Per estendere l'algoritmo di Tomasulo includendo meccanismi di hardware-speculat
ion è necessario separare la distribuzione dei risultati di un'esecuzione
 alle reservation-station dal completamento dell'istruzione stessa.
 Effettuando questa separazione possiamo fare in modo che istruzioni speculative
 non modifichino lo stato del sistema in modo permanente, rendendo possibile
 annullare le operazioni effettuate: solo quando un'istruzione cesserà di
 essere speculativa gli sarà consentito di aggiornare il register-file o
 la memoria.
 Questo step addizionale è detto 
\series bold
\emph on
instruction-commit
\series default
\emph default
.
\end_layout

\begin_layout Standard
L'idea è quella di consentire alle istruzioni di eseguire out-order, ma
 forzare il commit in-order, al fine di prevenire situazioni in cui le istruzion
i speculative effettuino operazioni permanenti e non annullabili, come la
 modifica della memoria o la generazione di eccezioni.
 In sostanza si separa l'istante in cui un'istruzione termina la sua esecuzione
 dall'istante in cui essa può scrivere nei registri o nella memoria, ossia
 effettuare il commit.
 
\end_layout

\begin_layout Standard
Dal momento che tra l'istante in cui un'istruzione termina la sua esecuzione
 ed il momento in cui essa smette di essere speculativa, e diventa possibile
 effettuare il commit, può passare anche molto tempo, aggiungere la fase
 di commit richiede l'aggiunta di hardware addizionale per mantenere i risultati
 delle istruzioni che abbiano terminato l'esecuzione ma che non abbiano
 ancora eseguito il commit.
 Questo hardware aggiuntivo viene chiamato 
\emph on
reorder-buffer
\emph default
, e, oltre che per ripristinare l'ordine di esecuzione delle istruzioni,
 viene utilizzato anche per il passaggio dei risultati da un'istruzione
 all'altra quando esse sono ancora speculative.
 Il motivo per cui il reorder-buffer deve essere usato anche per lo scambio
 dei dati è semplice: nello schema di Tomasulo classico ogni istruzione
 potrà trovare i risultati di esecuzione delle istruzioni precedenti nel
 register-file, ma con l'hardware-speculation il register file non viene
 aggiornato fino a quando l'istruzione non effettua il commit, per cui deve
 essere necessariamente il reorder-buffer a fornire gli operandi alle istruzioni
, prima che venga effettuato il commit.
\end_layout

\begin_layout Standard
Ciascuna delle entry di un reorder-buffer contiene quattro campi:
\end_layout

\begin_layout Itemize
il tipo di istruzione, che indica se l'istruzione sia un branch (quindi
 non ha registro di destinazione), una operazione di store (la cui destinazione
 è un indirizzo di memoria) o una operazione tra registri (come una operazione
 aritmetica);
\end_layout

\begin_layout Itemize
l'identificativo del registro destinazione, o l'indirizzo di memoria di
 destinazione per una operazione di store;
\end_layout

\begin_layout Itemize
il flag ready;
\end_layout

\begin_layout Itemize
il campo valore, utilizzato per tenere il risultato dell'esecuzione dell'istruzi
one fino al commit.
\end_layout

\begin_layout Standard
In figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Tomasulo-ROB"

\end_inset

 viene mostrato lo schema di un processore che aggiunga il reorder-buffer
 allo schema di Tomasulo.
\end_layout

\begin_layout Standard
È importante precisare che le operazioni di store continuano ad essere eseguite
 in due passi, ma, in questo caso, il secondo passo è effettuato soltanto
 dopo il commit.
 Si noti, inoltre, che le reservation-station sono ancora necessarie per
 mantenere gli operandi delle istruzioni tra l'istante in cui viene effettuato
 l'issue e l'istante in cui se ne comincia l'esecuzione.
 Le reservation-station contengono anche l'identificativo dell'entry del
 reorder-buffer assegnata a contenere i risultati dell'esecuzione di un'istruzio
ne.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\noindent
\align center
\begin_inset Graphics
	filename img/3_HardwareSpeculation/speculation_tomasulo.png
	scale 80

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Tomasulo-ROB"

\end_inset

schema di Tomasulo con reorder-buffer
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Microfasi di elaborazione
\end_layout

\begin_layout Standard
L'aver separato il completamento dell'esecuzione di un'istruzione dall'istante
 in cui ne viene effettuato il commit induce la necessità di aggiungere
 la fase di 
\emph on
commit
\emph default
 alle microfasi di elaborazione.
 Gli step che si susseguono durante l'esecuzione di un'istruzione sono:
\end_layout

\begin_layout Enumerate

\emph on
issue
\emph default
: viene prelevata un'istruzione dalla coda e
\end_layout

\begin_deeper
\begin_layout Enumerate
se è disponibile una reservation-station libera ed uno slot nel reorder-buffer,
 ne viene fatto l'issue;
\end_layout

\begin_deeper
\begin_layout Enumerate
se gli operandi sono disponibili nel register-file o nel reorder-buffer,
 essi vengono inviati alla reservation-station assieme all'identificativo
 dell'entry del reorder-buffer assegnato all'istruzione; il tag dell'entry
 del reorder-buffer viene utilizzato per identificare il risultato dell'esecuzio
ne dell'istruzione nel momento in cui essa viene spedita sul common-data-bus;
\end_layout

\begin_layout Enumerate
se uno o più operandi non sono ancora disponibili, le reservation-station
 monitorano il common-data-bus, attendendo l'operando (o gli operandi) mancanti;
\end_layout

\end_deeper
\begin_layout Enumerate
se non esistono reservation-station libere o entry libere nel reorder-buffer,
 allora è necessario mettere in stallo la pipe fin quando non ci sarà una
 reservation-station ed un entry nel reorder-buffer liberi.
\end_layout

\end_deeper
\begin_layout Enumerate
execute: quando gli operandi sono disponibili all'interno della reservation-stat
ion, l'operazione comincia l'esecuzione.
 Le istruzioni possono impiegare più cicli di clock per completare l'esecuzione;
\end_layout

\begin_layout Enumerate
write-result: quando i risultati dell'esecuzione di un'istruzione sono pronti,
 vengono scritti sul common-data-bus e memorizzati sia nel reorder-buffer
 che in ogni reservation-station che ne era in attesa.
 Nel caso di operazioni di store, se il dato da scrivere è pronto allora
 viene scritto nel reorder-buffer, altrimenti bisogna monitorare il common-data-
bus fino a quando il risultato della store non venga inviato in broadcast
 attraverso di esso.
\end_layout

\begin_layout Enumerate
commit: è lo step finale, dopo il quale un'istruzione ha completato effettivamen
te la sua esecuzione.
 A seconda dell'istruzione che raggiunge questa fase, esistono sequenze
 di azioni differenti che è necessario intraprendere: 
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Enumerate
quando un istruzione di branch correttamente predetta raggiungere la cima
 del reorder-buffer, essa smette di essere speculativa così come tutte le
 istruzioni successive, per cui ne viene effettuato il commit;
\end_layout

\begin_layout Enumerate
quando un'istruzione registro-registro raggiunge la cima del reorder-buffer
 ed il risultato della sua esecuzione è effettivamente presente all'interno
 del reorder-buffer, allora il risultato viene scritto nei registri e l'entry
 del reorder-buffer liberato;
\end_layout

\begin_layout Enumerate
quando a raggiungere la cima del reorder-buffer è un istruzione di store,
 il comportamento è simile al caso precedente, eccezion fatta per il fatto
 che bisogna scrivere in memoria, non nei registri;
\end_layout

\begin_layout Enumerate
quando, invece, a raggiungere la cima del reorder-buffer è un'istruzione
 di branch di cui è stata sbagliata la predizione, il reorder-buffer deve
 essere svuotato in quanto la speculazione si è rivelata sbagliata; in questo
 caso, dopo aver effettuato il flush del reorder-buffer, l'esecuzione viene
 fatta ripartire dal punto corretto;
\end_layout

\end_deeper
\begin_layout Standard
Si osservi che un processore che faccia utilizzo del reorder-buffer può
 comunque implementare un meccanismo di gestione precisa delle interruzioni,
 in quanto esse vengono sollevate soltanto nel momento in cui se ne effettua
 il commit: essendo il commit delle istruzioni effettuato in ordine, eventuali
 eccezioni verranno sollevate in ordine.
 Le eccezioni sollevate da istruzioni che non abbiano ancora effettuato
 il commit vengono memorizzate all'interno del reorder-buffer.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\noindent
\align center
\begin_inset Graphics
	filename img/3_HardwareSpeculation/tomasulo_algorithm_with_rob.png
	scale 90

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
algoritmo di Tomasulo con reorder-buffer
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard

\end_layout

\end_body
\end_document
