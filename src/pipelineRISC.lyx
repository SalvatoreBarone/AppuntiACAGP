#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass book
\begin_preamble
\frenchspacing
\usepackage{hyperref}
\usepackage{amsmath}
\newcommand{\ø}{\raisebox{.3ex}{\tiny$\; \bullet \;$}}
\renewcommand{\arraystretch}{1.25}
\usepackage{mathpazo}
\usepackage{color}
\usepackage{graphicx}
\usepackage{multicol}

\RequirePackage[dvipsnames]{xcolor} % [dvipsnames]
\definecolor{halfgray}{gray}{0.55} % chapter numbers will be semi transparent .5 .55 .6 .0
\definecolor{webgreen}{rgb}{0,.5,0}
\definecolor{webbrown}{rgb}{.6,0,0}
\definecolor{Maroon}{cmyk}{0, 0.87, 0.68, 0.32}
\definecolor{RoyalBlue}{cmyk}{1, 0.50, 0, 0}
\definecolor{Black}{cmyk}{0, 0, 0, 0}
\newcommand{\stitle}[1]{{\textbf{\color{RoyalBlue} #1}}}
\newcommand{\mail} [1]{{\href{mailto:#1}{#1}}}

\hypersetup{
    colorlinks=true, linktocpage=true, pdfstartpage=3, pdfstartview=FitV,%
    breaklinks=true, pdfpagemode=UseNone, pageanchor=true, pdfpagemode=UseOutlines,
    plainpages=false, bookmarksnumbered, bookmarksopen=true, bookmarksopenlevel=1,
    hypertexnames=true, pdfhighlight=/O,
    urlcolor=RoyalBlue, linkcolor=RoyalBlue, 
    citecolor=webgreen, 
    pdfauthor = {NOME COGNOME},
    pdfcreator  = {LaTeX with hyperref package},
    pdfproducer = {pdfeTeX},
    plainpages=false,
    pdfpagelabels
}
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language italian
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 10
\spacing single
\use_hyperref false
\papersize a4paper
\use_geometry false
\use_package amsmath 1
\use_package amssymb 2
\use_package cancel 1
\use_package esint 0
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 0
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Indice
\shortcut idx
\color #008000
\end_index
\leftmargin 3.5cm
\topmargin 3.5cm
\rightmargin 3.5cm
\bottommargin 3.5cm
\headsep 1cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip medskip
\quotes_language swedish
\papercolumns 1
\papersides 2
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
Il 
\series bold
pipelining
\series default
 è una tecnica implementativa dove multiple istruzioni sono sovrapposte
 durante l'esecuzione.
 Trae vantaggio dal parallelismo che esiste tra le azioni necessarie per
 eseguire un'istruzione.
 Il pipelining è al momento la tecnica chiave utilizzata per rendere le
 CPU più veloci.
\end_layout

\begin_layout Standard
Una pipeline è come una linea di montaggio.
 In una linea di montaggio dell'automobile, ci sono molti step, ognuno contribui
sce alla costruzione della vettura.
 Ogni step opera in parallelo con gli altri passaggi, anche se su un'altra
 vettura.
 In un computer pipeline, ogni step della pipeline completa una parte di
 un'istruzione.
 Come la linea di montaggio, diversi step stanno completando diverse parti
 di diverse istruzioni in parallelo; ognuno di questi passaggi è chiamato
 
\emph on
pipe stage
\emph default
.
 Le fasi sono collegate una alla successiva per formare una pipe.
 
\end_layout

\begin_layout Standard
In una linea di montaggio dell'automobile, il throughput è definito come
 il numero di automobili all'ora e viene determinato da quanto spesso un'auto
 completata esce dalla linea di montaggio.
 Allo stesso modo, il throughput di una pipeline di istruzione è determinato
 da quanto spesso un'istruzione esce dalla pipe.
 Poiché gli stadi della pipe sono collegati, tutti quanti devono essere
 pronti per procedere allo stesso tempo, proprio come avremmo bisogno in
 un catena di montaggio.
 Il tempo necessario per spostare un'istruzione un passo in avanti nella
 pipeline è un 
\emph on
ciclo di processore
\emph default
.
 Poiché tutte le fasi procedono allo stesso tempo, la lunghezza di un ciclo
 del processore è determinata dal tempo necessario per il più lento stadio
 della pipe, proprio come nel caso di una linea di montaggio auto dove lo
 step più lungo determinerà il tempo di avanzamento della catena.
 In un computer, questo ciclo di processore è di solito pari a 1 clock cycle
 (a volte è 2, raramente di più).
\end_layout

\begin_layout Standard
L'obiettivo del progettista di pipeline è quello di bilanciare la lunghezza
 di ogni stadio della pipeline, proprio come il progettista della linea
 di montaggio cerca di bilanciare il tempo per ogni step del processo.
 Se le fasi sono perfettamente equilibrate, allora il tempo per istruzione
 sul processore pipelined - assumendo condizioni ideali - è uguale a:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\frac{\text{Tempo per istruzione su una macchina senza pipelining}}{\text{Numero di stadi della pipe}}
\]

\end_inset


\end_layout

\begin_layout Standard
In queste condizioni, la velocità della pipeline equivale al numero di fasi
 della pipe, proprio come una linea di montaggio con n stadi può idealmente
 produrre automobili n volte più velocemente.
 Di solito, tuttavia, gli stadi non sono perfettamente equilibrati.
 Inoltre, la pipelining comporta un certo overhead.
 Così, il tempo per istruzione sul processore pipeline non avrà il suo valore
 minimo possibile, ma può essere vicino ad esso.
 La pipelining produce una riduzione del tempo medio di esecuzione per istruzion
e.
 A seconda di ciò che si considera come base, la riduzione può essere considerat
a come il decremento del numero di cicli di clock per istruzione (
\emph on
CPI
\emph default
) o come diminuzione del tempo di ciclo del clock.
 Ad esempio, se un processore richiede più cicli di clock per istruzione,
 la pipelining vine vista come una riduzione del CPI.
 Se invece un processore richiede un ciclo di clock (lungo) per istruzione,
 la pipelining diminuisce il tempo di ciclo del clock.
 
\end_layout

\begin_layout Standard
Il pipelining è una tecnica di implementazione che sfrutta il parallelismo
 tra le istruzioni in un flusso di istruzioni sequenziale.
 Ha il vantaggio sostanziale che, a differenza di altre tecniche, non è
 visibile al programmatore.
 
\end_layout

\begin_layout Section
Basi del RISC instruction set
\end_layout

\begin_layout Standard
In questa sezione introduciamo il core di una tipica architettura RISC (l'archit
ettura RISC predefinita dei nostri esempi è il MIPS).
 
\end_layout

\begin_layout Standard
Le architetture RISC sono caratterizzate da alcune proprietà chiave che
 semplificano drasticamente la loro implementazione: 
\end_layout

\begin_layout Itemize
Tutte le operazioni sui dati si applicano ai dati nei registri e in genere
 modificano l'intero registro (32 o 64 bit per registro).
 
\end_layout

\begin_layout Itemize
Le uniche operazioni che influenzano la memoria sono le operazioni di load
 e store che spostano dati dalla memoria in un registro o alla memoria da
 un registro.
 Le operazioni di caricamento e memorizzazione che caricano o memorizzano
 meno di un registro completo (ad esempio, un byte, 16 bit o 32 bit) sono
 spesso disponibili.
 
\end_layout

\begin_layout Itemize
I formati di istruzione sono poche in numero, con tutte le istruzioni in
 genere in una dimensione.
 
\end_layout

\begin_layout Standard
Queste semplici proprietà portano a tante semplificazioni nell'implementazione
 della pipelining, ecco perché questi set di istruzioni sono stati progettati
 in questo modo.
 
\end_layout

\begin_layout Standard
Utilizziamo MIPS64, la versione a 64 bit dell'insieme di istruzioni MIPS.
 Le istruzioni a 64 bit estese sono generalmente designate avendo un D all'inizi
o o alla fine del mnemonico.
 Ad esempio DADD è la versione a 64 bit di un'istruzione di add, mentre
 LD è la versione a 64 bit di un'istruzione di load.
 Come le altre architetture RISC, l'insieme di istruzioni MIPS prevede 32
 registri, anche se il registro 0 ha sempre il valore 0.
 La maggior parte delle architetture RISC, come MIPS, possiede tre classi
 di istruzioni:
\end_layout

\begin_layout Enumerate

\emph on
Istruzioni ALU
\emph default
: Queste istruzioni richiedono due registri o un registro e un immediato
 con estensione del segno (chiamato istruzioni immediate ALU, hanno un offset
 a 16 bit in MIPS), operano su di essi, e memorizzano il risultato in un
 terzo registro.
 Le operazioni tipiche includono l'aggiunta (DADD), la sottrazione (DSUB)
 e le operazioni logiche (come AND o OR), che non differiscono tra le versioni
 a 32 e 64 bit.
 Le versioni immediate di queste istruzioni utilizzano le stesse mnemoniche
 con un suffisso di I.
 In MIPS, sono presenti forme signed e unsigned delle istruzioni aritmetiche.
 Le forme unsigned, che non generano eccezioni di overflow - e quindi sono
 le stesse in modalità a 32 bit e a 64 bit - hanno un U alla fine (ad esempio,
 DADDU, DSUBU, DADDIU).
\end_layout

\begin_layout Enumerate

\emph on
Istruzioni di load e store
\emph default
: Queste istruzioni prendono un registro sorgente, chiamato 
\emph on
registro di base, 
\emph default
e un campo immediate (16 bit in MIPS), chiamato 
\emph on
offset
\emph default
, come operandi.
 La somma, chiamata 
\emph on
indirizzo effettivo, 
\emph default
del contenuto del registro di base e l'offset con segno esteso viene utilizzata
 come indirizzo di memoria.
 Nel caso di un'istruzione di load, un secondo operando di registro funge
 da destinazione per i dati caricati dalla memoria.
 Nel caso di una store, il secondo operand register è la sorgente dei dati
 memorizzati in memoria.
 Le istruzioni load word (LD) e store word (SD) caricano o memorizzano l'intero
 contenuto del registro a 64 bit.
 
\end_layout

\begin_layout Enumerate

\emph on
Branch e jump
\emph default
: I branch sono trasferimenti condizionali di controllo.
 Di solito esistono due modi per specificare la condizione del ramo nelle
 architetture RISC: con una serie di bit di stato (a volte chiamati codice
 condizione) o da un insieme limitato di confronti tra una coppia di registri
 o tra un registro e uno zero.
 MIPS utilizza quest'ultimo approccio.
 In tutte le architetture RISC, la destinazione di un ramo è ottenuta aggiungend
o un offset con estensione di segno (16 bit in MIPS) al PC corrente.
 
\end_layout

\begin_layout Section
Implementazione di un RISC instruction set
\end_layout

\begin_layout Standard
Ogni istruzione in un sottoinsieme RISC può essere implementata in un massimo
 di 5 cicli di clock.
 I 5 cicli di clock sono i seguenti: 
\end_layout

\begin_layout Enumerate

\emph on
Instruction Fetch
\emph default
 (IF): Invia il program counter (PC) alla memoria e recupera l'istruzione
 corrente dalla memoria.
 Aggiorna il PC al prossimo PC sequenziale addizionando 4 (poiché ogni istruzion
e è 4 byte) al PC.
 
\end_layout

\begin_layout Enumerate

\emph on
Instruction decode/register fetch
\emph default
 (ID): decodifica l'istruzione e legge i registri corrispondenti ai registri
 sorgenti.
 Esegue il test di uguaglianza nei registri mentre vengono letti, per un
 eventuale branch.
 Calcola l'eventuale indirizzo di destinazione del branch aggiungendo l'offset
 con segno esteso al PC incrementato.
 La decodifica viene eseguita in parallelo con i registri di lettura, che
 è possibile perché gli specificatori dei registri sono in una posizione
 fissa in un'architettura RISC.
 Questa tecnica è conosciuta come decodifica 
\emph on
fixed-field
\emph default
.
\end_layout

\begin_layout Enumerate

\emph on
Execution/effective address cycle
\emph default
 (EX): L'ALU opera sugli operandi preparati nel ciclo precedente, eseguendo
 una delle tre funzioni a seconda del tipo di istruzione.
 
\end_layout

\begin_deeper
\begin_layout Enumerate
Riferimento memoria: l'ALU addiziona il registro base e l'offset per formare
 l'indirizzo effettivo.
\end_layout

\begin_layout Enumerate
Istruzione ALU registro-registro: l'ALU esegue l'operazione specificata
 dall'opcode ALU sui valori letti dal file di registro.
 
\end_layout

\begin_layout Enumerate
Istruzione ALU Registro - Immediato: l'ALU esegue l'operazione specificata
 dall'opcode ALU sul primo valore letto dal file di registro e l'immediato
 con estensione del segno.
 
\end_layout

\end_deeper
\begin_layout Enumerate

\emph on
Accesso alla memoria 
\emph default
(MEM): Se l'istruzione è una load, la memoria fa una lettura utilizzando
 l'indirizzo effettivo calcolato nel ciclo precedente.
 Se è una store, allora la memoria scrive i dati dal secondo registro letto
 dal file di registro utilizzando l'indirizzo effettivo.
 
\end_layout

\begin_layout Enumerate

\emph on
Write-Back 
\emph default
(WB): Istruzione ALU registro-registro o istruzione di load: scrive il risultato
 nel file di registro, proveniente dal sistema di memoria (per una load)
 o dall'ALU (per un'istruzione ALU).
 
\end_layout

\begin_layout Section
Pipeline a 5 stadi per processori RISC
\end_layout

\begin_layout Standard
Possiamo effettuare il pipeline dell'esecuzione descritta in precedenza
 con quasi nessun cambiamento semplicemente avviando una nuova istruzione
 ad ogni ciclo di clock.
 
\end_layout

\begin_layout Standard
Ognuno dei cicli di clock della sezione precedente diventa uno stadio della
 pipe - un ciclo nella pipe.
 Ciò determina il modello di esecuzione mostrato in Figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Pipeline-RISC"

\end_inset

, che è il modo tipico di disegnare una struttura pipelined.
 Anche se ogni istruzione richiede 5 cicli di clock per completare, durante
 ogni ciclo di clock l'hardware inizierà una nuova istruzione ed eseguirà
 una parte delle cinque diverse istruzioni.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename img/AppendixA_pipeline/01.png
	lyxscale 50
	scale 50

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Pipeline-RISC"

\end_inset

Pipeline RISC
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Potrebbe essere difficile credere che la pipelining sia così semplice; infatti
 non lo è.
 Per cominciare, dobbiamo determinare cosa succede in ogni ciclo di clock
 del processore e assicurarsi che non cerchiamo di eseguire due operazioni
 diverse con le stesse unità nello stesso ciclo di clock.
 Ad esempio, non è possibile richiedere ad una singola ALU di calcolare
 un indirizzo effettivo ed eseguire contemporaneamente un'operazione di
 sottrazione.
 Pertanto, dobbiamo assicurarci che la sovrapposizione delle istruzioni
 nella pipeline non possa causare un tale conflitto.
 Fortunatamente, la semplicità di un set di istruzioni RISC rende la valutazione
 delle risorse relativamente facile.
 
\end_layout

\begin_layout Standard
La Figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Pipeline-RISC-1"

\end_inset

 mostra una versione semplificata di un percorso di dati RISC disegnato
 in modo pipelined.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename img/AppendixA_pipeline/02.png
	lyxscale 50
	scale 50

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Pipeline-RISC-1"

\end_inset

Pipeline RISC
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Come si può vedere, le principali unità funzionali vengono utilizzate in
 cicli diversi e quindi sovrapponendo l'esecuzione di più istruzioni introduciam
o relativamente pochi conflitti.
 
\end_layout

\begin_layout Standard
Innanzitutto, utilizziamo memorie separate di istruzioni e dati, che normalmente
 implementeremo con istruzioni separate e cache di dati.
 L'utilizzo di cache separate elimina un conflitto per una singola memoria
 che si verificherebbe tra l'accesso alle istruzioni e l'accesso alla memoria
 dati.
 In secondo luogo, il file di registro viene utilizzato nelle due fasi:
 uno per la lettura in ID e uno per la scrittura in WB - questi usi sono
 distinti.
 Quindi dobbiamo eseguire due letture e scrivere ogni ciclo di clock.
 Per gestire le letture e una scrittura nello stesso registro eseguiamo
 il registro di scrittura nella prima metà del ciclo del clock e la lettura
 nella seconda metà.
 
\end_layout

\begin_layout Section
Problemi di prestazioni di base nel pipelining 
\end_layout

\begin_layout Standard
Il pipelining aumenta il throughput della istruzione della CPU - il numero
 di istruzioni completate per unità di tempo - ma non riduce il tempo di
 esecuzione di un'istruzione individuale.
 Infatti, di solito aumenta leggermente il tempo di esecuzione di ogni istruzion
e a causa del sovraccarico nel controllo della pipe.
 L'aumento del throughput di istruzione significa che un programma si esegue
 più velocemente e ha un tempo di esecuzione totale inferiore, anche se
 nessuna istruzione viene eseguita più velocemente.
 
\end_layout

\begin_layout Standard
Oltre alle limitazioni derivanti dalla latenza delle pipeline, i limiti
 derivano dallo squilibrio tra le fasi della pipe e dalla sovrapposizione
 delle pipeline.
 Lo squilibrio tra le fasi della pipe riduce le prestazioni dato che il
 clock non può essere eseguito più velocemente del tempo necessario per
 la fase più lenta della pipeline.
 L'overhead della pipeline proviene dalla combinazione del ritardo del registro
 della pipe e del clock skew.
 I registri della pipe, inoltre, comportano un tempo di configurazione.
 Il clock skew, che è il ritardo massimo tra quando il clock arriva a due
 registri, contribuisce anche al limite inferiore del ciclo di clock.
 
\end_layout

\begin_layout Subsection
Hazard
\end_layout

\begin_layout Standard
Ci sono situazioni, chiamate 
\series bold
hazard
\series default
, che impediscono l'esecuzione dell'istruzione successiva nel flusso di
 istruzioni durante il suo ciclo di clock designato.
 Gli hazards riducono le prestazioni della velocità di accelerazione ideale
 ottenuta tramite pipeline.
 
\end_layout

\begin_layout Standard
Esistono tre classi di hazard: 
\end_layout

\begin_layout Enumerate
Gli 
\emph on
hazard strutturali 
\emph default
derivano da conflitti di risorse quando l'hardware non supporta tutte le
 possibili combinazioni di istruzioni contemporaneamente durante l'esecuzione
 sovrapposta.
 
\end_layout

\begin_layout Enumerate
I 
\emph on
data hazard
\emph default
 si presentano quando un'istruzione dipende dai risultati di una precedente
 istruzione.
 
\end_layout

\begin_layout Enumerate
I 
\emph on
control hazard
\emph default
 derivano dal pipelining di branch ed altre istruzioni che modificano il
 PC.
 
\end_layout

\begin_layout Standard
Gli hazard nelle pipe possono rendere necessario lo stallo della pipe stessa.
 Evitare un hazard spesso richiede che ad alcune istruzioni nella pipe venga
 consentito di procedere mentre altre sono ritardate.
 
\end_layout

\end_body
\end_document
